import React from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  page: {
    backgroundColor: 'white',
  },
  pad: {
    paddingHorizontal: '5%',
  },
  inputText: {
    borderColor: 'gray',
    borderWidth: 1
  }
});


export default styles;